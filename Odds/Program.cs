﻿using DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace OddsScreen
{
    public class Program
    {
        private static string _requestUri = "https://localhost:44397/api/odds/";
        private static List<Odds> _odds = null;

        public static async Task Main()
        {
            Thread.Sleep(1000); //I'm aware that is not good.

            while (true)
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromMilliseconds(Timeout.Infinite);
                    await DisplayOdds(httpClient.GetStreamAsync(_requestUri).Result);
                }
            }
        }

        /// <summary>
        /// Display the odds
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private static async Task DisplayOdds(Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    List<Odds> newOdds = JsonConvert.DeserializeObject<List<Odds>>(await reader.ReadLineAsync());

                    if (_odds == null || !newOdds.All(_odds.Contains) || newOdds.Count != _odds.Count)
                    {
                        _odds = newOdds;
                        Console.Clear();

                        foreach (Odds odd in _odds)
                            Console.WriteLine(odd);
                    }
                }
            }
        }
    }
}