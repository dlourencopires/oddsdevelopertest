﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DAL.Utils
{
    /// <summary>
    /// Class to manage file that contains the odds
    /// </summary>
    public static class FileManager
    {
        private static readonly string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private const string FILE = @"file.csv";

        /// <summary>
        /// Load the file contents
        /// </summary>
        /// <returns>A list of odds</returns>
        public static List<Odds> LoadFile()
        {
            List<Odds> odds = new List<Odds>();

            string path = Path.Combine(_path, FILE);

            using (var reader = new StreamReader(File.OpenRead(FILE)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(';');

                    if (values == null || values.Count() == 0)
                    {
                        throw new FileLoadException("File is empty");
                    }

                    if (!int.TryParse(values[0], out int id) || !Enum.TryParse(values[3], out ResultEnum result))
                    {
                        throw new FileLoadException("File does not contain right values");
                    }

                    odds.Add(new Odds(id: id,
                                       first: values[1],
                                       second: values[2],
                                       result: result));
                }
            }

            return odds;
        }

        /// <summary>
        /// Write/Rewrite file
        /// </summary>
        /// <param name="odds"></param>
        public static void SaveFile(List<Odds> odds)
        {
            try
            {
                StringBuilder csv = new StringBuilder();

                foreach (Odds odd in odds)
                {
                    csv.AppendLine($"{odd.Id};{odd.First};{odd.Second};{odd.Result}");
                }

                File.WriteAllText(FILE, csv.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}