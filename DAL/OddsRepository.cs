﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    /// <summary>
    /// Perform repository actions for idds
    /// </summary>
    public class OddsRepository : IOddsRepository
    {
        private List<Odds> _odds;

        public OddsRepository()
        {
            _odds = Utils.FileManager.LoadFile();
        }

        /// <summary>
        /// Auxiliary method to feed some odds
        /// </summary>
        public void FeedOdds()
        {
            _odds = new List<Odds>()
            {
                new Odds(id:1, first:"Manchester", second:"Chelsea",     result:ResultEnum.Draw),
                new Odds(id:2, first:"Barcelona",  second:"Real Madrid", result:ResultEnum.FirstWon),
                new Odds(id:3, first:"Milan",      second:"Napoli",      result:ResultEnum.SecondWon),
            };
        }

        /// <summary>
        /// Get a list of odds
        /// </summary>
        /// <returns></returns>
        public virtual List<Odds> GetAsync() =>
            _odds;

        /// <summary>
        /// /Get an odd
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Odds GetAsync(int id) =>
            _odds.FirstOrDefault(odd => odd.Id == id);

        /// <summary>
        /// Delete an odd
        /// </summary>
        /// <param name="odd"></param>
        public virtual void Delete(Odds odd)
        {
            Odds foundOdd = GetAsync(odd.Id);
            if (foundOdd != null)
            {
                _odds.Remove(foundOdd);
                Utils.FileManager.SaveFile(_odds);
            }
            else
            {
                throw new ArgumentException("Odd not found");
            }
        }

        /// <summary>
        /// Add an odd
        /// </summary>
        /// <param name="odd"></param>
        public virtual void AddAsync(Odds odd)
        {
            _odds.Add(odd);
            Utils.FileManager.SaveFile(_odds);
        }

        /// <summary>
        /// Update an odd
        /// </summary>
        /// <param name="odd"></param>
        public virtual void Update(Odds odd)
        {
            Odds foundOdd = GetAsync(odd.Id);
            if (foundOdd != null)
            {
                _odds.Remove(foundOdd);
                _odds.Add(odd);
                Utils.FileManager.SaveFile(_odds);
            }
        }
    }
}