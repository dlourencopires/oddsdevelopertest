﻿using System.Collections.Generic;

namespace DAL
{
    /// <summary>
    /// Odds repository
    /// </summary>
    public interface IOddsRepository
    {
        void FeedOdds();

        List<Odds> GetAsync();

        Odds GetAsync(int id);

        void Delete(Odds odd);

        void AddAsync(Odds odd);

        void Update(Odds odd);
    }
}