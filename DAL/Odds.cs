﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL
{
    /// <summary>
    /// Maps an Odd
    /// </summary>
    public class Odds
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string First { get; set; }

        [Required]
        public string Second { get; set; }

        [Required]
        public ResultEnum Result { get; set; }

        public Odds(int id, string first, string second, ResultEnum result)
        {
            Id = id;
            First = first;
            Second = second;
            Result = result;
        }

        /// <summary>
        /// Rewrite string to this object
        /// </summary>
        /// <returns>string</returns>
        public override string ToString() =>
            $"For Id:{Id}, {First} x {Second} has the result {Result.ToString()}";

        /// <summary>
        /// Compare both objects using all their properties
        /// </summary>
        /// <param name="obj">An Odds object to be compared to</param>
        /// <returns>True if is equal or false if it's not</returns>
        public override bool Equals(object obj)
        {
            if (obj is Odds odd)
            {
                return Id.Equals(odd.Id)
                    && First.Equals(odd.First)
                    && Second.Equals(odd.Second)
                    && Result.Equals(odd.Result);
            }
            return false;
        }

        /// <summary>
        /// Override of the hashcode to comparison
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() =>
            HashCode.Combine(Id, First, Second, Result);
    }
}