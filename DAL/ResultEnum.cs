﻿namespace DAL
{
    /// <summary>
    /// Enum to map results
    /// </summary>
    public enum ResultEnum
    {
        Draw = 0,
        FirstWon = 1,
        SecondWon = 2
    }
}