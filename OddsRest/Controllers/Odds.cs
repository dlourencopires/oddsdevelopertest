﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using OddsRest.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OddsRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OddsController : ControllerBase
    {
        private readonly IOddsRepository _OddsRepository;

        public OddsController(IOddsRepository oddsRepository)
        {
            _OddsRepository = oddsRepository;
        }

        /// <summary>
        /// Get a list of odds
        /// </summary>
        /// <returns>A a list of odds</returns>
        /// <remarks>
        /// Usage Example:
        /// GET /api/odds
        ///
        /// Headers
        /// Accept: application/json
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<Odds>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        public async Task<ActionResult<List<Odds>>> Get()
        {
            return Ok(_OddsRepository.GetAsync());
        }

        /// <summary>
        /// Get one odd
        /// </summary>
        /// <returns>An odd</returns>
        /// <remarks>
        /// Usage Example:
        /// GET /api/odds/1
        ///
        /// Headers
        /// Accept: application/json
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="404">Not Found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet("{id}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(Odds))]
        [ProducesResponseType(statusCode: 404, Type = typeof(NotFoundResult))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        public async Task<ActionResult<Odds>> Get(int id)
        {
            var odd = _OddsRepository.GetAsync(id);

            if (odd == null)
            {
                return NotFound();
            }

            return Ok(odd);
        }

        /// <summary>
        /// Delete a odd
        /// </summary>
        /// <returns>No Content</returns>
        /// <remarks>
        /// Usage Example:
        /// POST /api/odds/1
        ///
        /// Headers
        /// Accept: application/json
        /// </remarks>
        /// <response code="204">No Content</response>
        /// <response code="404">Not Found</response>
        /// <response code="500">Internal Server Error</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(statusCode: 204, Type = typeof(NoContentResult))]
        [ProducesResponseType(statusCode: 404, Type = typeof(NotFoundResult))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        public async Task<ActionResult> Delete(int id)
        {
            var odd = _OddsRepository.GetAsync(id);

            if (odd == null)
            {
                return NotFound();
            }

            _OddsRepository.Delete(odd);

            return NoContent();
        }

        /// <summary>
        /// Update a odd
        /// </summary>
        /// <returns>No Content</returns>
        /// <remarks>
        /// Usage Example:
        /// POST /api/odds/1
        ///
        /// Headers
        /// Accept: application/json
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="404">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPut("{id}")]
        [ProducesResponseType(statusCode: 204, Type = typeof(NoContentResult))]
        [ProducesResponseType(statusCode: 400, Type = typeof(BadRequestResult))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        public async Task<ActionResult> Put(int id, [FromBody]Odds odd)
        {
            if (!ModelState.IsValid || id != odd.Id)
            {
                return BadRequest(ErrorResponse.FromModelState(ModelState));
            }

            _OddsRepository.Update(odd);

            return NoContent();
        }

        /// <summary>
        /// Create a odd
        /// </summary>
        /// <returns>An odd</returns>
        /// <remarks>
        /// Usage Example:
        /// POST /api/odds/
        ///
        /// Headers
        /// Accept: application/json
        /// </remarks>
        /// <response code="201">Created at action</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpPost]
        [ProducesResponseType(statusCode: 201, Type = typeof(CreatedAtActionResult))]
        [ProducesResponseType(statusCode: 400, Type = typeof(BadRequestObjectResult))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        public async Task<ActionResult<Odds>> Post([FromBody]Odds odd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ErrorResponse.FromModelState(ModelState));
            }

            _OddsRepository.AddAsync(odd);

            return CreatedAtAction(nameof(Get), new { id = odd.Id }, odd);
        }
    }
}