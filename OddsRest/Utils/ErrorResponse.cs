﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;

namespace OddsRest.Utils
{
    /// <summary>
    /// Class to handle errors
    /// </summary>
    public class ErrorResponse
    {
        public int Codigo { get; set; }
        public string Mensagem { get; set; }
        public ErrorResponse InnerError { get; set; }
        public string[] Detalhes { get; private set; }

        /// <summary>
        /// Get an Error Response
        /// </summary>
        /// <param name="e"><Exception/param>
        /// <returns>ErrorResponse</returns>
        public static ErrorResponse From(Exception e)
        {
            if (e == null) { return null; }

            return new ErrorResponse
            {
                Codigo = e.HResult,
                Mensagem = e.Message,
                InnerError = From(e.InnerException)
            };
        }

        /// <summary>
        /// Get error from model state
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns>ErrorResponse</returns>
        public static ErrorResponse FromModelState(ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(x => x.Errors);
            return new ErrorResponse
            {
                Codigo = 100,
                Mensagem = "An error has occurred.",
                Detalhes = errors.Select(x => x.ErrorMessage).ToArray()
            };
        }
    }
}