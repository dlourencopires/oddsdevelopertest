using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using OddsRest.Controllers;
using OddsRest.DAL;
using OddsRest.Models;
using OddRestTest.Helpers;

namespace OddRestTest
{
    public class OddRestTest
    {
        private OddsController _Controller;
        private IOddsRepository _OddsRepository;
        private readonly List<Odds> _odds;

        public OddRestTest()
        {
            _odds = new List<Odds>()
            {
                new Odds(id:1, first:"Manchester", second:"Chelsea",     result:ResultEnum.Draw),
                new Odds(id:2, first:"Barcelona",  second:"Real Madrid", result:ResultEnum.FirstWon),
                new Odds(id:3, first:"Milan",      second:"Napoli",      result:ResultEnum.SecondWon),
            };
        }

        [Fact]
        public async Task ShouldGetTheListOfOdds()
        {
            //Arrange
            _OddsRepository = new OddsRepository();
            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = await _Controller.Get();
            var odds = (result.Result as OkObjectResult).Value as List<Odds>;

            //Assert
            Assert.Equal(_odds, odds);
        }

        [Theory]
        [ClassData(typeof(OddsRequestData))]
        public async Task ShouldGetTheRightBandaByID(Odds expectedOdd)
        {
            //Arrange
            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = await _Controller.Get(expectedOdd.Id);
            var odd = (result.Result as OkObjectResult).Value as Odds;

            //Assert
            Assert.Equal(expectedOdd, odd);
        }

        [InlineData(5)]
        [InlineData(6)]
        public async Task ShouldGetNotFoundIfBandaISUnknown(int id)
        {
            //Arrange
            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = (await _Controller.Get(id)).Result as NotFoundResult;

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task ShouldAddANewBand()
        {
            //Arrange
            Odds expectedOdd = new Odds(id:4, first:"Botafogo", second:"Flamengo", result:ResultEnum.FirstWon);

            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = await _Controller.Post(expectedOdd);
            var odd = (result.Result as CreatedAtActionResult).Value as Odds;

            //Assert
            Assert.IsType<ActionResult<Odds>>(result);
            Assert.Equal(expectedOdd, odd);
        }

        [Fact]
        public async Task ShouldChangeABand()
        {
            //Arrange
            Odds expectedOdd = new Odds(id: 4, first: "Botafogo", second: "Flamengo", result: ResultEnum.FirstWon);

            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = await _Controller.Put(expectedOdd.Id, expectedOdd) as StatusCodeResult;

            //Assert
            Assert.IsType<StatusCodeResult>(result);
            Assert.Equal((int)HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact]
        public async Task ShouldRemoveABand()
        {
            //Arragne
            Odds expectedOdd = new Odds(id: 4, first: "Botafogo", second: "Flamengo", result: ResultEnum.FirstWon);
            _Controller = new OddsController(_OddsRepository);

            //Act
            var result = await _Controller.Delete(expectedOdd.Id) as StatusCodeResult;

            //Assert
            Assert.IsType<StatusCodeResult>(result);
            Assert.Equal((int)HttpStatusCode.NoContent, result.StatusCode);
        }
    }
};