﻿using OddsRest.Models;
using System.Collections;
using System.Collections.Generic;

namespace OddRestTest.Helpers
{
    public class OddsRequestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new Odds(id:1, first:"Manchester", second:"Chelsea",     result:ResultEnum.Draw),
                new Odds(id:2, first:"Barcelona",  second:"Real Madrid", result:ResultEnum.FirstWon),
                new Odds(id:3, first:"Milan",      second:"Napoli",      result:ResultEnum.SecondWon),
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}