﻿using DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utils;

namespace ManageOdds
{
    internal class Program
    {
        public static async Task Main()
        {
            ApiClient<Odds> apiClient = new ApiClient<Odds>();
            await DisplayOdds(apiClient);

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Choose an option: (1)Add, (2)Delete, (3)Update, Get(4)");

                if (!int.TryParse(Console.ReadLine(), out int option))
                {
                    Console.WriteLine("Invalid option.");
                }

                switch (option)
                {
                    case 1:
                        Console.WriteLine("Paste JSON here:");
                        await AddOdd(apiClient);

                        break;

                    case 2:
                        Console.WriteLine("Enter ID to be deleted:");
                        await DeleteOdd(apiClient);

                        break;

                    case 3:
                        Console.WriteLine("Enter json to be changed:");
                        await UpdateOdd(apiClient);

                        break;

                    case 4:
                        Console.WriteLine("Enter ID:");
                        await GetOdd(apiClient);

                        break;

                    default:
                        break;
                }

                Console.WriteLine();
                await DisplayOdds(apiClient);
            }
        }

        /// <summary>
        /// Get an odd
        /// </summary>
        /// <param name="apiClient"></param>
        /// <returns></returns>
        private static async Task GetOdd(ApiClient<Odds> apiClient)
        {
            try
            {
                int id;
                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("Invalid option: A number was expected.");
                    return;
                }

                Console.WriteLine((await apiClient.Get(id)).ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong when trying to get the odd. See: {e.Message}");
            }
        }

        /// <summary>
        /// Update an odd
        /// </summary>
        /// <param name="apiClient"></param>
        /// <returns></returns>
        private static async Task UpdateOdd(ApiClient<Odds> apiClient)
        {
            try
            {
                Odds odd = JsonConvert.DeserializeObject<Odds>(Console.ReadLine());
                await apiClient.Update(odd.Id, odd);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong when trying to update this odd. See: {e.Message}");
            }
        }

        /// <summary>
        /// Delete an odd
        /// </summary>
        /// <param name="apiClient"></param>
        /// <returns></returns>
        private static async Task DeleteOdd(ApiClient<Odds> apiClient)
        {
            try
            {
                int id;
                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("Invalid option: A number was expected.");
                    return;
                }

                await apiClient.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong when trying to delete this odd. See: {e.Message}");
            }
        }

        /// <summary>
        /// Add an odd
        /// </summary>
        /// <param name="apiClient"></param>
        /// <returns></returns>
        private static async Task AddOdd(ApiClient<Odds> apiClient)
        {
            try
            {
                Odds odd = JsonConvert.DeserializeObject<Odds>(Console.ReadLine());
                await apiClient.Save(odd);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong when trying to add this odd. See: {e.Message}");
            }
        }

        /// <summary>
        /// Display the odds
        /// </summary>
        /// <param name="apiClient"></param>
        /// <returns></returns>
        private static async Task DisplayOdds(ApiClient<Odds> apiClient)
        {
            Console.WriteLine("==============================================================================");
            List<Odds> odds = await apiClient.Get();

            if (odds == null || !odds.Any())
            {
                Console.WriteLine("No odds were found.");
                return;
            }

            foreach (Odds odd in odds)
            {
                Console.WriteLine(odd);
            }
        }
    }
}