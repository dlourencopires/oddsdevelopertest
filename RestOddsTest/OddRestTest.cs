using DAL;
using Microsoft.AspNetCore.Mvc;
using OddsRest.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OddRestTest
{
    public class OddRestTest
    {
        private OddsController _Controller;
        private IOddsRepository _OddsRepository;
        private readonly List<Odds> _odds;

        public OddRestTest()
        {
            _OddsRepository = new OddsRepository();
            _OddsRepository.FeedOdds();

            _Controller = new OddsController(_OddsRepository);
            _odds = new List<Odds>()
            {
                new Odds(id:1, first:"Manchester", second:"Chelsea",     result:ResultEnum.Draw),
                new Odds(id:2, first:"Barcelona",  second:"Real Madrid", result:ResultEnum.FirstWon),
                new Odds(id:3, first:"Milan",      second:"Napoli",      result:ResultEnum.SecondWon),
            };
        }

        /// <summary>
        /// Should get a list of odds
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ShouldGetTheListOfOdds()
        {
            //Act
            var result = await _Controller.Get();
            var odds = (result.Result as OkObjectResult).Value as List<Odds>;

            //Assert
            Assert.Equal(_odds.Count(), odds.Count());

            foreach (Odds odd in _odds)
            {
                Assert.Contains(odds, o => o.Equals(odd));
            }
        }

        /// <summary>
        /// Should get an odd by id
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task ShouldGetTheRightOddByID(int oddId)
        {
            //Act
            var result = await _Controller.Get(oddId);
            var odd = (result.Result as OkObjectResult).Value as Odds;

            //Assert
            Assert.IsType<ActionResult<Odds>>(result);
            Assert.Equal(oddId, odd.Id);
        }

        /// <summary>
        /// Should get not found if an odd is unknown
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(5)]
        [InlineData(6)]
        public async Task ShouldGetNotFoundIfOddISUnknown(int id)
        {
            //Act
            var result = (await _Controller.Get(id)).Result as NotFoundResult;

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Should check if a odd was removed
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ShouldRemoveAnOdd()
        {
            //Arragne
            var oddToBeDeleted = new Odds(id: 1, first: "Manchester", second: "Chelsea", result: ResultEnum.SecondWon);

            //Act
            var result = await _Controller.Delete(oddToBeDeleted.Id) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
            Assert.Equal((int)HttpStatusCode.NoContent, result.StatusCode);
        }

        /// <summary>
        /// Should check if an odd was created
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ShouldAddANewOdd()
        {
            //Arrange
            Odds expectedOdd = new Odds(id: 4, first: "Botafogo", second: "Flamengo", result: ResultEnum.FirstWon);

            //Act
            var result = await _Controller.Post(expectedOdd);
            var odd = (result.Result as CreatedAtActionResult).Value as Odds;

            //Assert
            Assert.IsType<ActionResult<Odds>>(result);
            Assert.Equal(expectedOdd, odd);
        }

        /// <summary>
        /// Should check if an odd was changed
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ShouldChangeAnOdd()
        {
            //Arrange
            var oddToBeChanged = new Odds(id: 1, first: "Manchester", second: "Chelsea", result: ResultEnum.SecondWon);

            //Act
            var result = await _Controller.Put(oddToBeChanged.Id, oddToBeChanged) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
            Assert.Equal((int)HttpStatusCode.NoContent, result.StatusCode);
        }
    }
};