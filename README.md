---

## Run application

Using Visual Studio, just press shift + F5 to run application.
Manage odds and screen odds consoles will appear at your monitor.

---

## Manage odds

In this screen you can add, update, get and delete odds:

Add: To add just choose this option and paste a valid json, for example:
{"Id":8, "First":"Botafogo", "Second": "Flamengo", "Result":"FirstWon"}

Update: To update just choose this option and paste a already valid json object, for example:
{"Id":1, "First":"Manchester", "Second": "Chelsea", "Result":"SecondWon"}

Delete: To delete just choose this option and enter a valid Id, for example:
1

Get: To get an odds, just choose this option and enter a valid Id, for example:
1

*All changes are automatically pushed to the client screen.