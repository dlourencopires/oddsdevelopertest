---

## ABOUT TIME
I spent around 16H to do that.

## ABOUT VERSION CONTROL
I did have some problems with proxy that's why I had to push all the branches later. 

## ABOUT DESIGN

I choose to work with REST API to avoid duplicated code and too its easily implementation.
I have a lot of experience working as a full stack developer, but I choose console applications only to follow the test documentation and to save time, just for that.
I don't have a lot of experience dealing with real time application only because I didn't have the opportunity. I wondering to work with SignalR but for a console application didn't work, I also made using websockets but I was losing a lot of time to get done perfectly, so I choose a simple solution that I know that I can't apply ona daily-basis job.
I also didn't do it the button to publish at the screen through manager screen. I did automatically 'cause I just missed this information, but I can do without problems, if you guys wishes.

## ABOUT TESTING

I did TDD all the time.  
I avoid the use of Moqs just to save time. I'm currently working with moqs but I didn't see the need for that here.
Exceptions tests are missing here, I could do that but dind't have enought time.

## ABOUT PERSISTENCE

I choose to have a simple csv file to manage the odds to avoid dealing with the database for this POC. I have a lot of experience with SQL server and some with MySQL, Postrgee, Oracle and few with MongoDB.


