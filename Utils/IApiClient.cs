﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Utils
{
    /// <summary>
    /// To handle api calls
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IApiClient<T> where T : class
    {
        Task<List<T>> Get();

        Task<T> Get(int id);

        Task<T> Save(T model);

        Task<T> Update(int id, T model);

        Task<T> Delete(int id);
    }
}