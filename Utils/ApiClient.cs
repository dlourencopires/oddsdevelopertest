﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Utils
{
    /// <summary>
    /// Handle API calls
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiClient<T> : IApiClient<T> where T : class
    {
        private readonly HttpClient _HttpClient;
        private readonly string _Type;
        private HttpResponseMessage _HttpResponseMessage;

        public ApiClient()
        {
            const string API_ADDRESS = "https://localhost:44397/api/";
            _HttpClient = new HttpClient
            {
                BaseAddress = new Uri(API_ADDRESS)
            };

            _Type = typeof(T).Name;
        }

        /// <summary>
        /// Call a delete for an id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> Delete(int id)
        {
            _HttpResponseMessage = await _HttpClient.DeleteAsync($"{_Type}/{id.ToString()}");
            _HttpResponseMessage.EnsureSuccessStatusCode();

            return await _HttpResponseMessage.Content.ReadAsAsync<T>();
        }

        /// <summary>
        /// Call a get without parameters
        /// </summary>
        /// <returns></returns>
        public async Task<List<T>> Get()
        {
            _HttpResponseMessage = await _HttpClient.GetAsync(_Type);
            _HttpResponseMessage.EnsureSuccessStatusCode();

            return await _HttpResponseMessage.Content.ReadAsAsync<List<T>>();
        }

        /// <summary>
        /// Call a get for an id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> Get(int id)
        {
            _HttpResponseMessage = await _HttpClient.GetAsync($"{_Type}/{id.ToString()}");
            _HttpResponseMessage.EnsureSuccessStatusCode();

            return await _HttpResponseMessage.Content.ReadAsAsync<T>();
        }

        /// <summary>
        /// Call a post
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<T> Save(T model)
        {
            _HttpResponseMessage = await _HttpClient.PostAsync($"{_Type}", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            _HttpResponseMessage.EnsureSuccessStatusCode();

            return await _HttpResponseMessage.Content.ReadAsAsync<T>();
        }

        /// <summary>
        /// Call an update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<T> Update(int id, T model)
        {
            _HttpResponseMessage = await _HttpClient.PutAsync($"{_Type}/{id.ToString()}", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            _HttpResponseMessage.EnsureSuccessStatusCode();

            return await _HttpResponseMessage.Content.ReadAsAsync<T>();
        }
    }
}